# Control node
resource "aws_instance" "control" {
  ami             = var.image
  instance_type   = var.instance
  #private_ip      = 
  user_data       = file("postinstall.sh")
  vpc_security_group_ids = [aws_security_group.base.id]

  tags = {
    Owner = var.owner
    Name  = "control"
  }
}

# Worker node 1
resource "aws_instance" "worker1" {
  ami             = var.image
  instance_type   = var.instance
  #private_ip      = 
  user_data       = file("postinstall.sh")
  vpc_security_group_ids = [aws_security_group.base.id]

  tags = {
    Owner = var.owner
    Name  = "worker1"
  }
}

# Worker node 2
resource "aws_instance" "worker2" {
  ami             = var.image
  instance_type   = var.instance
  #private_ip      = 
  user_data       = file("postinstall.sh")
  vpc_security_group_ids = [aws_security_group.base.id]

  tags = {
    Owner = var.owner
    Name  = "worker2"
  }
}

# Worker node 3
resource "aws_instance" "worker3" {
  ami             = var.image
  instance_type   = var.instance
  #private_ip      = 
  user_data       = file("postinstall.sh")
  vpc_security_group_ids = [aws_security_group.base.id]

  tags = {
    Owner = var.owner
    Name  = "worker3"
  }
}
